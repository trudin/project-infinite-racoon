package com.infiniteracoon;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.util.Random;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;

public class CreateProfileActivity extends DrawerActivity {
	

	private final static int SELECT_PHOTO = 100;
	private boolean isDone = false;
	private ImageView profileImage;
	private ParseUser profile;
	private ParseFile image;
	private Bitmap thumbImage;
	private ParseFile parseThumbImage;
	
	
	private OnClickListener buttonListener = new OnClickListener() {

		@Override
		public void onClick(View v) {

			switch (v.getId()) {
			case R.id.createprofile_addphoto_button:
				// Launch user photo gallery and lets user pick one.
				Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
				photoPickerIntent.setType("image/*");
				startActivityForResult(photoPickerIntent, SELECT_PHOTO);
				break;

			case R.id.createprofile_decline_button:
				finish();
				break;

			case R.id.createprofile_accept_button:
				if (isDone) {
					// TODO - spara användare till databasen.
					saveProfile();
				}
				break;

			default:
				break;
			}
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_profile);
		
	    Toolbar toolbar = (Toolbar) findViewById(R.id.create_profile_toolbar);
	    setSupportActionBar(toolbar);
	    getSupportActionBar().setDisplayShowTitleEnabled(false);
	    
		// Set onClickListeners
		findViewById(R.id.createprofile_accept_button).setOnClickListener(buttonListener);
		findViewById(R.id.createprofile_decline_button).setOnClickListener(buttonListener);
		findViewById(R.id.createprofile_addphoto_button).setOnClickListener(buttonListener);
		
		profileImage = (ImageView) findViewById(R.id.createprofile_image);
		thumbImage = BitmapFactory.decodeResource(getResources(),
                R.drawable.profile);
		
		final EditText profileNameET = (EditText) findViewById(R.id.createprofile_name_edit);
		profileNameET.setOnFocusChangeListener(new OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
					if (profileNameET.getText().toString().length() < 3) {
						profileNameET.setError("Namnet måste vara minst 3 tecken");
					} else {
						profileNameET.setError(null);
					}
				}
			}
		});
		
		final EditText profileEmailET = (EditText) findViewById(R.id.createprofile_email_edit);
		profileEmailET.setOnFocusChangeListener(new OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
					String email = profileEmailET.getText().toString();
					if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
						profileEmailET.setError("Känner inte igen email-formatet.");
						isDone = false;
					} else {
						profileEmailET.setError(null);
						isDone = true;
					}
				}	
			}
		});
		
		final EditText profilePasswordET = (EditText) findViewById(R.id.createprofile_password_edit);
		profilePasswordET.setOnFocusChangeListener(new OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
					if (profilePasswordET.getText().toString().length() < 6) {
						profilePasswordET.setError("Lösenordet måste vara minst 6 tecken");
						isDone = false;
					} else {
						profilePasswordET.setError(null);
						isDone = true;
					}
				}
			}
		});
		
		final EditText profileConfirmET = (EditText) findViewById(R.id.createprofile_confirm_password_edit);
		profileConfirmET.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
				if (!profileConfirmET.getText().toString().equals(profilePasswordET.getText().toString())) {
					profileConfirmET.setError("Lösenorden stämmer inte överens");
					isDone = false;
				} else {
					profileConfirmET.setError(null);
					isDone = true;
				}
			}
			
			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}
			
			@Override
			public void afterTextChanged(Editable arg0) {}
		});
	}



	/**
	 * Takes care of the chosen image. Downsamples and shows it in the view.
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {		
		super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

		switch (requestCode) {
		case SELECT_PHOTO:
			if (resultCode == RESULT_OK) {

				ImageDownsampler imgDs = new ImageDownsampler(this,
						imageReturnedIntent.getData());
				try {
					Bitmap selectedImage = imgDs.decodeUri(300);

					profileImage.setImageBitmap(selectedImage);
					
					thumbImage = imgDs.decodeUri(80);

				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void saveProfile() {
		profile = new ParseUser();
		profile.setUsername(((EditText) findViewById(R.id.createprofile_name_edit)).getText().toString());
		profile.setEmail(((EditText) findViewById(R.id.createprofile_email_edit)).getText().toString());
		profile.setPassword(((EditText) findViewById(R.id.createprofile_password_edit)).getText().toString());
		profile.put("age", Integer.parseInt(((EditText) findViewById(R.id.createprofile_age_edit)).getText().toString()));

		Bitmap bitmap = ((BitmapDrawable)profileImage.getDrawable()).getBitmap();
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		bitmap.compress(CompressFormat.PNG, 0, bos);
		byte[] bitmapdata = bos.toByteArray();
		image = new ParseFile("profileimage.png",bitmapdata);
		
		bos = new ByteArrayOutputStream();
		thumbImage.compress(CompressFormat.PNG, 0, bos);
		bitmapdata = bos.toByteArray();
		parseThumbImage = new ParseFile("profileimage_thumb.png",bitmapdata);
		parseThumbImage.saveInBackground();
		
		image.saveInBackground(new SaveCallback() {
			
			@Override
			public void done(ParseException e) {
				if (e == null) {
					signUp();
				}
			}
		});
	}
	
	private void signUp() {
		profile.put("profile_image_thumb", parseThumbImage);
		profile.put("profile_image", image);
		profile.signUpInBackground(new SignUpCallback() {
			@Override
			public void done(ParseException e) {
				if (e == null) {
					Toast.makeText(CreateProfileActivity.this, "Profil skapad",
							Toast.LENGTH_SHORT).show();
					finish();
				} else {
					Toast.makeText(CreateProfileActivity.this, "Misslyckades med att skapa profil: "+e.getMessage(),
							Toast.LENGTH_LONG).show();
				}
			}
		});
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.create_profile, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		if (mDrawerToggle.onOptionsItemSelected(item)) {
	        return true;
	    }
		return super.onOptionsItemSelected(item);
	}
}
