package com.infiniteracoon;

import java.util.ArrayList;

import android.graphics.Bitmap;

import com.parse.ParseGeoPoint;

public class Group {


	private String groupname;
	private int distance;
	private String interests;
	private static final String KM = "km";
	private double longitude, latitude;
	private ParseGeoPoint location;
	private ArrayList<String> hashtags;
	private int adminId;
	private Bitmap thumbnail;


	public Group(String name, String information, double lat, double lng, String hashtags) {
		this.groupname = name;
		this.interests = information;
		this.latitude = lat;
		this.longitude = lng;
		(this.hashtags = new ArrayList<String>()).add(hashtags);

	}

	public Group(String groupname, ParseGeoPoint location, String interests){ 
		this.groupname=groupname;
		this.location=location;
		this.interests=interests;

	}

	public Group() {

	}

	public String getGroupname() {
		return groupname;
	}

	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}

	public int getDistance() {
		return distance;
	}

	public void setDistance(int distance) {
		this.distance = distance;
	}

	public String getInterests() {
		return interests;
	}

	public void setInterests(String interests) {
		this.interests = interests;
	}

	public String getKm() {
		return KM;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public ArrayList<String> getHashtags() {
		return hashtags;
	}

	public void setHashtags(ArrayList<String> hashtags) {
		this.hashtags = hashtags;
	}

	public int getAdminId() {
		return adminId;
	}

	public void setAdminId(int adminId) {
		this.adminId = adminId;
	}

	public Bitmap getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(Bitmap thumbnail) {
		this.thumbnail = thumbnail;
	}

	public ParseGeoPoint getLocation() {
		return location;
	}

	public void setLocation(ParseGeoPoint location) {
		this.location = location;
	}

}
