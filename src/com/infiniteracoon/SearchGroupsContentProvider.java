package com.infiniteracoon;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.app.SearchManager;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

public class SearchGroupsContentProvider extends android.content.ContentProvider {

	
	@Override
	public boolean onCreate() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		
		// Uri:n i metodargumentet innehåller användarens sökord som sista segment.
		String searchString = uri.getLastPathSegment();
		
		// Måste returnera en cursor som pekar på de objekt som ska synas bland suggestions
		Cursor c = null;
		
		// Så vi inte anropar databasen när användaren knappt börjat.
		if (searchString.length() > 2 && !searchString.equals("search_suggest_query")) {
			c = callDatabase(searchString);
		}
		
		return c;
	}

	private MatrixCursor callDatabase(String searchString) {
		// Cursor kan inte instantieras så vi använder en MatrixCursor för att bygga suggestions-tabellen
		MatrixCursor mc = new MatrixCursor(new String[]{ "_id", SearchManager.SUGGEST_COLUMN_ICON_1, 
																SearchManager.SUGGEST_COLUMN_TEXT_1, 
																SearchManager.SUGGEST_COLUMN_QUERY});
				
		// Kontakta databasen och se om vi får några bra träffar på hashtags.
	    ParseQuery<ParseObject> query = ParseQuery.getQuery("Hashtag");
	    query.whereStartsWith("hashtag", searchString);
	    
	    // Skapar en lista som ska innehålla alla träffar som parseobjekt
	    List<ParseObject> group = null;
		try {
			group = query.find(); // Vet inte om såna här content providers automatiskt 
								// körs i annan tråd men annars kanske vi bör hantera detta annorlunda
					
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		
	    int i = 0;
	    // Om vi hittat något
		if (!group.isEmpty()) {
			// Loopa igenom träffarna och formatera cursor-kolumnerna. 
			// Parametrarna är: [1] _id så android kan bygga en listview, [2] icon, [3] text som visas som suggestion, [4] söksträng som skickas i intentet
			for (ParseObject po : group) {
				String suggestion = po.getString("hashtag");
				
				String intentQuery = "#" + suggestion;
				mc.addRow(new String[] { Integer.toString(i), "android.resource://com.infiniteracoon/" + R.drawable.ic_hashtag_24, suggestion, intentQuery });
				i++;				
			}			
		}	
		// Vi söker också efter adresser
		Geocoder geocoder = new Geocoder(getContext());
		List<Address> addresses;
		try {
			addresses = geocoder.getFromLocationName(searchString, 2);
			for (Address address : addresses) {
				String intentQuery = "@" + address.getLatitude() + "@" + address.getLongitude();
				mc.addRow(new String[] { Integer.toString(i), "android.resource://com.infiniteracoon/" 
										+ R.drawable.ic_room_white_24dp, address.getAddressLine(0) + ", " + address.getAddressLine(1), intentQuery });			
			} 
		} catch (IOException e) {
			e.printStackTrace();
		}		
		
		return mc;
	}

	@Override
	public String getType(Uri uri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		// TODO Auto-generated method stub
		return 0;
	}

}
