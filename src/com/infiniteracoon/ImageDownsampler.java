package com.infiniteracoon;

import java.io.FileNotFoundException;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;

public class ImageDownsampler {
	Context context;
	Uri selectedImage;
	
	public ImageDownsampler(Context context, Uri selectedImage) {
		this.context = context;
		this.selectedImage = selectedImage;
	}
	
	/**
	 * Handles memory by scaling the image until it's within the bounds of requiredSize.
	 * @param requiredSize - width and height/2 can't be higher than this number
	 * @return a subsampled Bitmap.
	 * @throws FileNotFoundException
	 */
	public Bitmap decodeUri(int requiredSize) throws FileNotFoundException {

        // Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        
        // Minneshantering - bitmapen sätts till null men man kan fortfarande ändra i bilden - inget minne
        // behöver allokeras till pixlarna än.
        o.inJustDecodeBounds = true;
        
        // Här får vi bilden som användaren har valt. Kommer från onActivityResult. getContentResolver behövde ett context
        // så det är anledningen till att jag har skickat med det i konstruktorn
        BitmapFactory.decodeStream(context.getContentResolver().openInputStream(selectedImage), null, o);

        // Find the correct scale value. It should be the power of 2.
        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp / 2 < requiredSize
               || height_tmp / 2 < requiredSize) {
                break;
            }
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        // Decode with inSampleSize.
        // Här 
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(context.getContentResolver().openInputStream(selectedImage), null, o2);

    }

}
