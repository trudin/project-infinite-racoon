package com.infiniteracoon;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;


public class ProfileActivity extends DrawerActivity implements 
GooglePlayServicesClient.ConnectionCallbacks,
GooglePlayServicesClient.OnConnectionFailedListener {

	private LocationClient locClient;
	private Location location;
	private int thumbCounter = 0;
	private ArrayList<Group> groups  = new ArrayList<Group>();



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_profile);

	    locClient = new LocationClient(this, this, this);

		Toolbar toolbar = (Toolbar) findViewById(R.id.profile_toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayShowTitleEnabled(false);

		ParseUser user = ParseUser.getCurrentUser();

		((TextView)findViewById(R.id.profile_name_text)).setText(user.getUsername());
		((TextView)findViewById(R.id.profile_email_text)).setText(user.getEmail());
		((TextView)findViewById(R.id.profile_age_text)).setText("" + user.getInt("age"));
		ParseFile groupImage = (ParseFile) ParseUser.getCurrentUser().get("profile_image");
		groupImage.getDataInBackground(new GetDataCallback() {

			@Override
			public void done(byte[] data, ParseException e) {
				if (e == null) {
					showProfilePicture(data);
				}
			}
		});

		ParseRelation<ParseObject> relation = ParseUser.getCurrentUser().getRelation("group");
		relation.getQuery().findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> objects, ParseException e) {

				if  (objects.size() > 0) {
					thumbCounter = 0;

					for (int i = 0; i < objects.size(); i++) {
						groups.add(new Group(
								objects.get(i).getString("name"),
								objects.get(i).getParseGeoPoint("location"),
								objects.get(i).getString("description")));
					}
					for (int i = 0; i < objects.size(); i++) {

						((ParseFile) objects.get(i).get("group_image_thumb")).getDataInBackground(new GetDataCallback() {
							
							int j = thumbCounter;

							@Override
							public void done(byte[] data, ParseException e) {
								if (e == null) {									 
									showImage(data, j);
								}
							}
						});
						thumbCounter++;
					}
				}
				for (int i = 0; i < groups.size(); i++) {
					ParseGeoPoint point = new ParseGeoPoint(location.getLatitude(), location.getLongitude());
					groups.get(i).setDistance((int)(point.distanceInKilometersTo(groups.get(i).getLocation()) + 0.5));
				}
			}
		});

	}

	private void showProfilePicture(byte[] data) {
		Bitmap bitmap = BitmapFactory.decodeByteArray(data , 0, data.length);
		((ImageView)findViewById(R.id.profile_image)).setImageBitmap(bitmap);
	}

	private void showImage(byte[] data, int i) {
		Bitmap bitmap = BitmapFactory.decodeByteArray(data , 0, data.length);
		groups.get(i).setThumbnail(bitmap);
		adapter();
	}
	
	private void adapter() {
		ProfileListViewGroupsAdapter glva= new ProfileListViewGroupsAdapter(ProfileActivity.this,groups);
		glva.notifyDataSetChanged();
		ListView listViewOnProfile= (ListView)findViewById(R.id.profile_listview);
		listViewOnProfile.setAdapter(glva);
		registerClickCallback();
	}

	//TODO make onItemClick work on listView
	private void registerClickCallback() {
		// TODO Auto-generated method stub
		ListView glva = (ListView) findViewById(R.id.profile_listview);
		glva.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View viewClicked, int position, long id) {
				// TODO Auto-generated method stub
				View v = (View) viewClicked;

				TextView textview =(TextView)v.findViewById(R.id.title_text);
				goToGroup(textview.getText().toString());

				//String message = "You clicked # "+position+textview.getText();

				//	Toast.makeText(ProfileActivity.this, message,Toast.LENGTH_SHORT).show();
			}

		});

	}
	

	private void goToGroup(String groupName) {
		// TODO Auto-generated method stub
		Intent goToGroup = new Intent(this,GroupActivity.class); 
		goToGroup.putExtra("name", groupName);
		startActivity(goToGroup);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.profile, menu);
		return true;
	}	

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	protected void onStart() {
		super.onStart();
		locClient.connect();
	}

	protected void onStop() {
		super.onStop();
		locClient.disconnect();
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onConnected(Bundle connectionHint) {
		location = locClient.getLastLocation();
	}

	@Override
	public void onDisconnected() {
		// TODO Auto-generated method stub
		
	}

}
