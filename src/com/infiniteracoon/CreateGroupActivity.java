package com.infiniteracoon;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.parse.SaveCallback;

public class CreateGroupActivity extends DrawerActivity {

	// TODO - databas-variabel.
	private Group group;
	private final static int SELECT_PHOTO = 100;
	protected final static String LATITUDE = "lat";
	protected final static String LONGITUDE ="lng";
	protected final static String NAME = "name";
	private ParseObject parseGroup;
	private EditText groupNameET, groupHashtagsET, groupAddressET, groupDescriptionET;
	private ImageView groupImageIV;
	private boolean isDone = false;
	private Bitmap thumbImage;

	// Kartvariabler
	private Geocoder geocoder;
	private LatLng latLng = new LatLng(0.0, 0.0); // Defaultvärde för testning
	private List<Address> addresses;
	private List<String> hashtags = new ArrayList<String>();

	private OnClickListener buttonListener = new OnClickListener() {

		@Override
		public void onClick(View v) {

			switch (v.getId()) {
			case R.id.creategroup_addphoto_button:
				// Launch user photo gallery and lets user pick one.
				Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
				photoPickerIntent.setType("image/*");
				startActivityForResult(photoPickerIntent, SELECT_PHOTO);
				break;

			case R.id.creategroup_decline_button:
				finish();
				break;

			case R.id.creategroup_accept_button:

				if (isDone) {
					checkName();
					checkHashtag();
					createGroup();
					saveGroup();
				}
				break;

			default:
				break;
			}
		}
	};

	/**
	 * Takes care of the chosen image. Downsamples and shows it in the view.
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {		
		super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

		switch (requestCode) {
		case SELECT_PHOTO:
			if (resultCode == RESULT_OK) {

				ImageDownsampler imgDs = new ImageDownsampler(this,
						imageReturnedIntent.getData());
				try {
					Bitmap selectedImage = imgDs.decodeUri(300);

					groupImageIV.setImageBitmap(selectedImage);
					
					thumbImage = imgDs.decodeUri(80);

				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Save the edittext input to Group object.
	 */
	private void createGroup() {
		group.setGroupname(groupNameET.getText().toString());


		getLatLngFromAddress(groupAddressET.getText().toString());
		group.setLatitude(latLng.latitude);
		group.setLongitude(latLng.longitude);

		group.setInterests(groupDescriptionET.getText().toString());

		group.setHashtags(null);
	}

	/**
	 * Sends the group object to database.
	 */
	private void saveGroup() {
		ParseGeoPoint location = new ParseGeoPoint();
		location.setLatitude(group.getLatitude());
		location.setLongitude(group.getLongitude());
		parseGroup = new ParseObject("Group");
		parseGroup.put("location", location);
		parseGroup.put("name", group.getGroupname());
		parseGroup.put("description", group.getInterests());
		parseGroup.put("admin", ParseUser.getCurrentUser());
		parseGroup.put("address", addresses.get(0).getAddressLine(0)+addresses.get(0).getAddressLine(1));
		

		Bitmap bitmap = ((BitmapDrawable)groupImageIV.getDrawable()).getBitmap();
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		bitmap.compress(CompressFormat.PNG, 0, bos);
		byte[] bitmapdata = bos.toByteArray();
		ParseFile parseImage = new ParseFile("groupimage.png",bitmapdata);
		parseImage.saveInBackground();
		parseGroup.put("group_image", parseImage);
		
		bos = new ByteArrayOutputStream();
		thumbImage.compress(CompressFormat.PNG, 0, bos);
		bitmapdata = bos.toByteArray();
		ParseFile parseThumbImage = new ParseFile("groupimage_thumb.png",bitmapdata);
		parseThumbImage.saveInBackground();
		parseGroup.put("group_image_thumb", parseThumbImage);

		parseGroup.saveInBackground(new SaveCallback() {

			@Override
			public void done(ParseException e) {
				if (e == null) {
					ParseQuery<ParseObject> query = ParseQuery.getQuery("Hashtag");

					query.whereContainedIn("hashtag", hashtags);
					
					query.findInBackground(new FindCallback<ParseObject>() {
						
						@Override
						public void done(List<ParseObject> objects, ParseException e) {
							for (int i = 0; i < hashtags.size(); i++) {
								ParseObject parseHashtag = new ParseObject("Hashtag");
								boolean found = false;
								for (int j = 0; j < objects.size(); j++) {
									if (objects.get(j).getString("hashtag").equals(hashtags.get(i))) {
										parseHashtag = objects.get(j);
										ParseRelation<ParseObject> relation = parseHashtag.getRelation("groups");
										relation.add(parseGroup);
										parseHashtag.saveInBackground();
										found = true;
									}
								}
								if (!found) {
									parseHashtag.put("hashtag", hashtags.get(i));
									ParseRelation<ParseObject> relation = parseHashtag.getRelation("groups");
									relation.add(parseGroup);
									parseHashtag.saveInBackground();
								}
							}
						}
					});
					ParseUser user = ParseUser.getCurrentUser();
					ParseRelation<ParseObject> relation = user.getRelation("group");
					relation.add(parseGroup);
					user.saveInBackground();
					
					Toast.makeText(CreateGroupActivity.this, "Gruppen skapad", Toast.LENGTH_SHORT).show();

					Intent resultIntent = new Intent();
					resultIntent.putExtra(LATITUDE, group.getLatitude());
					resultIntent.putExtra(LONGITUDE, group.getLongitude());
					resultIntent.putExtra(NAME, group.getGroupname());
					setResult(CreateGroupActivity.RESULT_OK, resultIntent);
					finish();
				} else {
					Toast.makeText(CreateGroupActivity.this,  "Misslyckades att skapa gruppen: " + e.getMessage(), Toast.LENGTH_LONG).show();
				}
			}
		});
	}

	private void getLatLngFromAddress(String inputAddress) {

		try {
			addresses = geocoder.getFromLocationName(inputAddress, 1);

			if (!addresses.isEmpty()) {
				Address address = addresses.get(0);
				latLng = new LatLng(address.getLatitude(),
						address.getLongitude());
			} else {
				// TODO - hantera fel inslagna adresser.
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_group);

		Toolbar toolbar = (Toolbar) findViewById(R.id.create_group_toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayShowTitleEnabled(false);

		// If MapActivity starts this activity it will pass position-info
		// (last-known location or user selected marker) with the intent.
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			Double latitude = extras.getDouble("com.infiniteracoon.LATITUDE");
			Double longitude = extras.getDouble("com.infiniteracoon.LONGITUDE");
			latLng = new LatLng(latitude, longitude);
		}
		
		groupImageIV = (ImageView) findViewById(R.id.create_group_image);

		groupAddressET = (EditText) findViewById(R.id.creategroup_address_edit);
		groupDescriptionET = (EditText) findViewById(R.id.creategroup_description_edit);

		groupNameET = (EditText)findViewById(R.id.creategroup_name_edit);
		groupNameET.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
					checkName();
				}
			}
		});

		groupHashtagsET = (EditText)findViewById(R.id.creategroup_hashtags_edit);
		groupHashtagsET.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
					checkHashtag();
				}
			}
		});

		// TODO - hantera aktiv användare på något sätt. här bara gjort en ny
		// användare. Man kan ju tänka sig att man bara skickar med ett adminId i intentet
		// men man lär ju vilja använda användarens uppgifter lite överallt i appen.

		// Setup variables.
		group = new Group();
		geocoder = new Geocoder(this);
		thumbImage = BitmapFactory.decodeResource(getResources(),
                R.drawable.group);
		

		// Set onClickListeners
		findViewById(R.id.creategroup_accept_button).setOnClickListener(buttonListener);
		findViewById(R.id.creategroup_decline_button).setOnClickListener(buttonListener);
		findViewById(R.id.creategroup_addphoto_button).setOnClickListener(buttonListener);

		// pre-sets address to user position.
		setAddress();

	}

	private void checkHashtag() {
		groupHashtagsET.setError(null);
		hashtags.clear();
		String hashtag = new String();
		String userInput = groupHashtagsET.getText().toString().toLowerCase();
		
		if (userInput.length() != 0) {
			for (int i = 0; i < userInput.length(); i++) {
				switch (userInput.charAt(i)) {
				case ' ':
					hashtags.add(hashtag);
					hashtag = new String();
					break;
				case '#':
					break;
				default:
					hashtag = hashtag + userInput.charAt(i);
					if (i == userInput.length()-1) {
						hashtags.add(hashtag);
					}
				}
			}
		}
	}

	private void checkName() {
		if (groupNameET.getText().toString().length() < 3) {
			groupNameET.setError("Namnet måste vara minst 3 tecken");
			isDone = false;
		} else {
			groupNameET.setError(null);
			isDone = true;
		}
	}

	private void setAddress() {
		try {
			addresses = geocoder.getFromLocation(latLng.latitude,
					latLng.longitude, 1);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (!addresses.isEmpty()) {

			Address address = addresses.get(0);
			String street = address.getAddressLine(0);
			String city = address.getAddressLine(1);
			groupAddressET.setText(street + ", " + city);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.create_group, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		if (mDrawerToggle.onOptionsItemSelected(item)) {
	        return true;
	    }
		return super.onOptionsItemSelected(item);
	}
}
