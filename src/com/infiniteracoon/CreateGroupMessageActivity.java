package com.infiniteracoon;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;

public class CreateGroupMessageActivity extends ActionBarActivity {

	private String groupName;

	private OnClickListener buttonListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			//checks witch button is pressed
			switch (v.getId()) {
			case R.id.createmessage_decline_button:
				finish();
				break;

			case R.id.createmessage_accept_button:
				saveMessage();
				break;

			default:
				break;
			}
		}
	};



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_groupmessage);

		groupName = getIntent().getExtras().getString("com.infiniteracoon.GROUP_NAME");

		((TextView)findViewById(R.id.createmessage_groupname)).setText(groupName);
		//set toolbar
		Toolbar toolbar = (Toolbar) findViewById(R.id.create_groupmessage_toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayShowTitleEnabled(false);

		// Set onClickListeners
		findViewById(R.id.createmessage_accept_button).setOnClickListener(buttonListener);
		findViewById(R.id.createmessage_decline_button).setOnClickListener(buttonListener);


	}
	/**
	 * saves message to database
	 */
	private void saveMessage(){
		ParseObject message = new ParseObject("Post");
		message.put("title", ((EditText)findViewById(R.id.createmessage_title_edit)).getText().toString());
		message.put("message", ((EditText)findViewById(R.id.createmessage_message_edit)).getText().toString());
		message.put("author", ParseUser.getCurrentUser());
		message.put("group", groupName);
		message.saveInBackground(new SaveCallback() {

			@Override
			public void done(ParseException e) {
				if (e == null) {
					Toast.makeText(CreateGroupMessageActivity.this, "Inlägget postat", Toast.LENGTH_LONG).show();
					finish();
				} else {
					Toast.makeText(CreateGroupMessageActivity.this, "Inlägget ej postat: " + e.getMessage(), Toast.LENGTH_LONG).show();
				}
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_create_groupmessage, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
