package com.infiniteracoon;

import java.util.ArrayList;
import java.util.List;

import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseRelation;
import com.parse.ParseUser;

import android.support.v4.app.DialogFragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class DrawerActivity extends ActionBarActivity 
	implements ListView.OnItemClickListener, LoginListener{

	private DrawerLayout drawerLayout;
	private ListView menuListView;
	private ListView groupListView;
	private String[] menuAlternatives;
	private ArrayList<String> userGroups = new ArrayList<String>();
	protected ActionBarDrawerToggle mDrawerToggle;
	private FrameLayout activityContent;
	private ParseUser currentUser;
	private ArrayAdapter<String> menuAdapter;
	private ArrayAdapter<String> groupAdapter;
	private ImageView userImage;
	private TextView userName;
	
    @Override
    public void setContentView(final int layoutResID) {
        drawerLayout = (DrawerLayout) getLayoutInflater().inflate(R.layout.activity_drawer, null); // Your base layout here
        activityContent = (FrameLayout) drawerLayout.findViewById(R.id.activity_content);
        getLayoutInflater().inflate(layoutResID, activityContent, true); // Setting the content of layout your provided to the act_content frame
        super.setContentView(drawerLayout);
        

        // here you can get your drawer buttons and define how they should behave and what must they do, so you won't be needing to repeat it in every activity class
        
        drawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
		menuListView = (ListView)findViewById(R.id.drawer_menu_list);
		groupListView = (ListView)findViewById(R.id.drawer_group_list);
		menuAlternatives = getResources().getStringArray(R.array.alternatives);
		mDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.app_name, R.string.app_name) {


	            /** Called when a drawer has settled in a completely closed state. */
	            public void onDrawerClosed(View view) {
	                super.onDrawerClosed(view);
	               
	            }

	            /** Called when a drawer has settled in a completely open state. */
	            public void onDrawerOpened(View drawerView) {
	                super.onDrawerOpened(drawerView);
	               
	            }
		};		
		
		currentUser = ParseUser.getCurrentUser();		
		
		setProfileImage();
		setProfileGroups();
		
		
		if (currentUser == null) {
			menuAlternatives[3] = "Logga in";
		} else { 
			menuAlternatives[3] = "Logga ut";
		}		
		
		menuAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, menuAlternatives);
		
		// MenyLista i arrayadapter.
		menuListView.setAdapter(menuAdapter);
		menuListView.setOnItemClickListener(this);
		setGroupList();
		drawerLayout.setDrawerListener(mDrawerToggle);
    }

	private void setGroupList() {
		groupAdapter = new ArrayAdapter<String>(this, 
				android.R.layout.simple_list_item_1, userGroups);
		groupListView.setAdapter(groupAdapter);
		groupListView.setOnItemClickListener(this);
	}

	private void setProfileGroups() {
		currentUser = ParseUser.getCurrentUser();
		if (currentUser!=null) {	
			ParseRelation<ParseObject> relation = currentUser.getRelation("group");
			relation.getQuery().findInBackground(new FindCallback<ParseObject>() {

				@Override
				public void done(List<ParseObject> objects, ParseException e) {

					if  (objects.size() > 0) {	
						
						for (int i = 0; i < objects.size(); i++) {
							userGroups.add(objects.get(i).getString("name"));
							
						}		
						setGroupList();
						drawerUpdated();
						
					}
				}
			});
			
		}
	}

	private void setProfileImage() {
		userName = (TextView)findViewById(R.id.drawer_user_text);
		userImage = (ImageView)findViewById(R.id.drawer_user_image);
		currentUser = ParseUser.getCurrentUser();
		if (currentUser != null) {
			
			userName.setText((String)currentUser.get("username"));
			ParseFile profileImage = (ParseFile) currentUser.get("profile_image");
			profileImage.getDataInBackground(new GetDataCallback() {
	
				@Override
				public void done(byte[] data, ParseException e) {
					if (e == null) {
						Bitmap bitmap = BitmapFactory.decodeByteArray(data , 0, data.length);
						userImage.setImageBitmap(bitmap);
						drawerUpdated();
					}
				}
			});
		} else {
			removeProfileImage();
		}
	}

	private void removeProfileImage() {
		userName = (TextView)findViewById(R.id.drawer_user_text);
		userImage = (ImageView)findViewById(R.id.drawer_user_image);
		userImage.setImageDrawable(getResources().getDrawable(R.drawable.profile));
		userName.setText(null);
	}
	
	private void goToActivity(Class<?> cls) {
		Intent i = new Intent(this, cls);
		startActivity(i);	
		drawerLayout.closeDrawers();
	}
	
    public void showNoticeDialog() {
        // Create an instance of the dialog fragment and show it
        DialogFragment dialog = new LoginDialogFragment(this, this);
        dialog.show(getSupportFragmentManager(), "NoticeDialogFragment");
        
    }
	
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
	    super.onPostCreate(savedInstanceState);
	    // Sync the toggle state after onRestoreInstanceState has occurred.
	    mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
	    super.onConfigurationChanged(newConfig);
	    mDrawerToggle.onConfigurationChanged(newConfig);
	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		if (parent.getId() == R.id.drawer_group_list) {
			groupClicked(position);
		} else if(parent.getId() == R.id.drawer_menu_list) {
			menuClicked(position);
		}
	
	}

	private void menuClicked(int position){
			switch (position) {
			case 0:
				goToActivity(MapActivity.class);
				break;
			case 1:			
				if (ParseUser.getCurrentUser() != null) {
					goToActivity(ProfileActivity.class);
				} else showNoticeDialog();
				break;
			case 2:			
				if (ParseUser.getCurrentUser() != null) {
					goToActivity(CreateGroupActivity.class);
				} else showNoticeDialog();
				break;
			case 3:
				if (ParseUser.getCurrentUser() != null) {
					ParseUser.logOut();
					onParseLogout();
				} else showNoticeDialog();
				break;
			default:
				break;
		}
	}

	private void groupClicked(int position) {

		Intent i = new Intent(this, GroupActivity.class);
		i.putExtra("name", userGroups.get(position));
		startActivity(i);
		drawerLayout.closeDrawers();
		
	}

	@Override
	public void onParseLogin() {
		
		menuAlternatives[3] = "Logga ut";
		setProfileGroups();
		setProfileImage();
	}

	@Override
	public void onParseLogout() {
		
		Toast.makeText(this, "Du är utloggad", Toast.LENGTH_SHORT).show();
		menuAlternatives[3] = "Logga in";
		userGroups.clear();
		removeProfileImage();
		drawerUpdated();
		
	}

	private void drawerUpdated() {
		groupAdapter.notifyDataSetChanged();
		menuAdapter.notifyDataSetChanged();
		drawerLayout.postInvalidate();
	}
}
