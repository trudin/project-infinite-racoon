package com.infiniteracoon;

public class Profile {
	
	private String name;
	private String mail;
	
	// Hur lagra detta på ett säkert sätt?
	private String password;
	
	// detta bör man nog låta databasen generera så det 
	// inte kan bli dubbelt men sen vill man ju kunna använda 
	// det i koden för adminrättigheter och så. 
	private int userId;
	
	
	public Profile() {
		
	}
	
	public Profile(int userId, String name) {
		this.setUserId(userId);
		this.name = name;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getMail() {
		return mail;
	}


	public void setMail(String mail) {
		this.mail = mail;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public int getUserId() {
		return userId;
	}


	public void setUserId(int adminId) {
		this.userId = adminId;
	}

}
