package com.infiniteracoon;

public interface LoginListener {
	public void onParseLogin();
	public void onParseLogout();
}
