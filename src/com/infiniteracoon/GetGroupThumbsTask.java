package com.infiniteracoon;

import java.util.ArrayList;
import java.util.List;

import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

public class GetGroupThumbsTask extends AsyncTask<Void, Void, ArrayList<Bitmap>> {

	ArrayList<ParseObject> markersToThumbUp;
	ThumbsDownloadedListener listener;
	
	public GetGroupThumbsTask(List<ParseObject> visibleMarkers, ThumbsDownloadedListener listener) {
		this.markersToThumbUp = (ArrayList<ParseObject>) visibleMarkers;
		this.listener = listener;
	}

	@Override
	protected ArrayList<Bitmap> doInBackground(Void... args) {
		
		ArrayList<Bitmap> thumbs = new ArrayList<Bitmap>();
		
		for (ParseObject g : markersToThumbUp) {
			ParseFile groupImage = (ParseFile) g.getParseFile("group_image_thumb");
			try {
				byte[] data = groupImage.getData();
				if (data != null) {
					Bitmap thumb = BitmapFactory.decodeByteArray(data, 0, data.length);
					thumbs.add(thumb);
				}
			} catch (ParseException e) {}	
			
			
		}
		return thumbs;		
	}
	
	@Override
	protected void onPostExecute(ArrayList<Bitmap> thumbs) {
		if (!thumbs.isEmpty()) {
			listener.thumbsDownsloaded(markersToThumbUp, thumbs);
		}
	}
}
