package com.infiniteracoon;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class CommentsListViewAdapter extends BaseAdapter {

	private ArrayList<MessageComment> dataSource;  //Data input, change to DB
	private LayoutInflater inflater; 	


	/**
	 * contructor gets an activity and an arraylist of comments
	 * @param activity - get the activty
	 * @param comments - arraylist of comment
	 */
	public CommentsListViewAdapter(Activity activity, ArrayList<MessageComment> comments){
		dataSource = comments;
		inflater=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return dataSource.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return dataSource.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		View view= null;

		if(convertView==null){
			view=inflater.inflate(R.layout.comments, parent, false);
		}else {
			view=convertView;
		}


		MessageComment mc = dataSource.get(position);

		//sätter textviews i viewn
		TextView textView=(TextView)view.findViewById(R.id.comment_surename);
		textView=(TextView)view.findViewById(R.id.comment_lastname);
		textView=(TextView)view.findViewById(R.id.comment_message);
		return view;
	}

}
