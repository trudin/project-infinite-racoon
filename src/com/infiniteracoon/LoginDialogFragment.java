package com.infiniteracoon;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

public class LoginDialogFragment extends DialogFragment {

	private Context context;
	private View v;
	private LoginListener listener;

	public LoginDialogFragment(Context context, LoginListener listener) {
		super();
		this.context = context;
		this.listener = listener;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		// Get the layout inflater
		LayoutInflater inflater = getActivity().getLayoutInflater();

		// Inflate and set the layout for the dialog
		// Pass null as the parent view because its going in the dialog layout
		v = inflater.inflate(R.layout.signin_dialog, null);
		v.findViewById(R.id.register).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(context, CreateProfileActivity.class);
				startActivity(intent);
				dismiss();
			}
		});
		builder.setView(v)
				// Add action buttons
				.setPositiveButton(R.string.signin,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int id) {
							}
						})
				.setNegativeButton(R.string.cancel,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								LoginDialogFragment.this.getDialog().cancel();
							}
						});
		return builder.create();
	}

	//Override för att inte dialogrutan ska försvinna
	//när man trycker på Logga in och man inte loggas in
	@Override
	public void onStart() {
		super.onStart();
		AlertDialog d = (AlertDialog) getDialog();
		if (d != null) {

			final EditText usernameET = (EditText) v
					.findViewById(R.id.username);
			final EditText passwordET = (EditText) v
					.findViewById(R.id.password);
			Button positiveButton = (Button) d
					.getButton(Dialog.BUTTON_POSITIVE);
			positiveButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					ParseUser.logInInBackground(
							usernameET.getText().toString(), passwordET
									.getText().toString(), new LogInCallback() {
								public void done(ParseUser user,
										ParseException e) {
									whenDone(user);
						}
					});
				}
			});
		}
	}

	private void whenDone(ParseUser user) {
		if (user != null) {
			dismiss();
			Toast.makeText(context, "Du är inloggad", Toast.LENGTH_LONG).show();
			listener.onParseLogin();
		} else {
			Toast.makeText(context, "Användaren finns inte", Toast.LENGTH_LONG)
					.show();
		}
	}
}