package com.infiniteracoon;

import java.util.ArrayList;

import android.graphics.Bitmap;

import com.parse.ParseObject;

public interface ThumbsDownloadedListener {
	public void thumbsDownsloaded(ArrayList<ParseObject> groups, ArrayList<Bitmap> thumbs);
}
