package com.infiniteracoon;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ProfileListViewGroupsAdapter extends BaseAdapter {
	private ArrayList<Group> dataSource;  //Data input
	private LayoutInflater inflater; 	
	
	
	public ProfileListViewGroupsAdapter(Activity activity, ArrayList<Group> groups){
		dataSource = groups;
		inflater =(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}


	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return dataSource.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return dataSource.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
			View view= null;
		
		if(convertView==null){
			view=inflater.inflate(R.layout.profile_listview, parent, false);
		}else {
			view=convertView;
		}
		
		Group group = dataSource.get(position);
		
		ImageView imageView = (ImageView)view.findViewById(R.id.group_picture);
		imageView.setImageBitmap(group.getThumbnail());
		
		TextView textView = (TextView)view.findViewById(R.id.title_text);
		textView.setText(group.getGroupname());
		
		textView = (TextView)view.findViewById(R.id.short_info_text); 
		textView.setText(group.getInterests());
		
		textView = (TextView)view.findViewById(R.id.distance_text); 
		textView.setText(group.getDistance()+group.getKm());
		return view;
	}
	

}
