package com.infiniteracoon;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class GroupListViewMessageAdapter extends BaseAdapter{

	private ArrayList<GroupMessageInfo> dataSource;  
	private LayoutInflater inflater; 
	ArrayList<MessageComment> mc = new ArrayList<MessageComment>();

	/**
	 * gets the activity and arraylist of messages
	 * @param activity - 
	 * @param groupMessages - arraylist of messages
	 */
	public GroupListViewMessageAdapter(Activity activity, ArrayList<GroupMessageInfo> groupMessages){
		dataSource = groupMessages;
		mc.add(new MessageComment("kommentar kommentar kommentar kommentar kommentar kommentar"));
		inflater =(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return dataSource.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return dataSource.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}


	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view= null;

		if(convertView==null){
			view=inflater.inflate(R.layout.group_listview, parent, false);

		}else {
			view=convertView;
		}

		GroupMessageInfo groupmessage = dataSource.get(position);


		TextView comments = (TextView)view.findViewById(R.id.message_comments_text);
		comments.setText(groupmessage.getComments());

		ImageView imageView = (ImageView)view.findViewById(R.id.profile_message_image);
		imageView.setImageBitmap(groupmessage.getThumb());

		TextView textView = (TextView)view.findViewById(R.id.profile_surename_message_text);
		textView.setText(groupmessage.getSurename());

		textView = (TextView)view.findViewById(R.id.subject_message_text); 
		textView.setText(groupmessage.getTitle());

		textView = (TextView)view.findViewById(R.id.message_content_text); 
		textView.setText(groupmessage.getMessage());

		textView = (TextView)view.findViewById(R.id.date_time_text); 
		textView.setText(groupmessage.getDate());

		LinearLayout showcomments = (LinearLayout)view.findViewById(R.id.comments_group_message_linearlayout);

		textView = (TextView)showcomments.findViewById(R.id.comment_surename);
		textView.setText("Anders");

		return view;
	}
}



