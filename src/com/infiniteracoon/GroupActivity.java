package com.infiniteracoon;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.parse.SaveCallback;

public class GroupActivity extends DrawerActivity {

	static final String GROUP_NAME = "com.infiniteracoon.GROUP_NAME";
	private String groupName;
	private ParseObject parseGroup;
	private ArrayList<GroupMessageInfo> gmi;
	private GroupListViewMessageAdapter glvma;
	private ListView listViewMessage;
	private int messageCounter;
	private boolean member = false;
	private Button joinLeaveGroup;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_group);

		joinLeaveGroup = (Button)findViewById(R.id.joingroup_button);

		listViewMessage = (ListView)findViewById(R.id.group_message_listView);
		//get the toolbar
		Toolbar toolbar = (Toolbar) findViewById(R.id.my_awesome_toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayShowTitleEnabled(false);

		groupName = getIntent().getExtras().getString("name");

		// button for adding messages in group
		//Skapa ett query som sedan exekveras i bakgrunden. Söker i tabellen "Group" där "name" är lika
		//med group.getGroupname()
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Group");
		query.whereEqualTo("name", groupName);
		query.findInBackground(new FindCallback<ParseObject>() {

			//När queryn är klar körs "done" i huvudtråden
			@Override
			public void done(List<ParseObject> group, ParseException e) {
				// TODO Auto-generated method stub
				if (e == null) {
					showGroup(group);
				}
			}
		});

		query = ParseQuery.getQuery("Event");
		query.whereEqualTo("group", groupName);
		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				if (e == null) {
					if (objects.size() > 0) {
						((TextView)findViewById(R.id.event_info)).setText(objects.get(0).getString("message"));
					}
				}

			}
		});
	}

	private void showGroup(List<ParseObject> group) {
		parseGroup = group.get(0);
		if (ParseUser.getCurrentUser() != null) {
			ParseRelation<ParseObject> relation = ParseUser.getCurrentUser().getRelation("group");
			ParseQuery<ParseObject> query = relation.getQuery();
			query.whereEqualTo("name", groupName);
			query.findInBackground(new FindCallback<ParseObject>() {

				@Override
				public void done(List<ParseObject> objects, ParseException e) {
					if (objects.size() > 0) {
						member();
					}
				}
			});
		}


		ParseFile groupImage = (ParseFile) parseGroup.get("group_image");
		groupImage.getDataInBackground(new GetDataCallback() {

			@Override
			public void done(byte[] data, ParseException e) {
				if (e == null) {
					showImage(data);
				}
			}
		});
		((TextView)findViewById(R.id.groupname_text)).setText(parseGroup.getString("name"));
		((TextView)findViewById(R.id.address_text)).setText(parseGroup.getString("address"));
		((TextView)findViewById(R.id.description_text)).setText(parseGroup.getString("description"));




		findViewById(R.id.createmessage_addmessage_button).setOnClickListener(buttonListener);
		findViewById(R.id.joingroup_button).setOnClickListener(buttonListener);
		findViewById(R.id.add_event_button).setOnClickListener(buttonListener); 


		// temporary arraylist to get info for messages
		getMessagesFromParse();


		// connect adapter arraylist

	}

	private void getMessagesFromParse() {
		gmi = new ArrayList<GroupMessageInfo>();
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Post");
		query.whereEqualTo("group", groupName);
		query.addDescendingOrder("createdAt");
		query.include("author");
		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				if (e == null) {
					if (objects.size() > 0) {
						for (messageCounter = 0; messageCounter < objects.size(); messageCounter++) {
							ParseUser author = (ParseUser)objects.get(messageCounter).get("author");
							gmi.add(new GroupMessageInfo(author.getUsername(),
									"",
									objects.get(messageCounter).get("title").toString(),
									objects.get(messageCounter).get("message").toString(),
									objects.get(messageCounter).getCreatedAt().toString(),
									""));
							ParseFile thumb = (ParseFile) author.get("profile_image_thumb");
							thumb.getDataInBackground(new GetDataCallback() {

								int i = messageCounter;

								@Override
								public void done(byte[] data, ParseException e) {
									Bitmap thumb = BitmapFactory.decodeByteArray(data, 0, data.length);
									gmi.get(i).setThumb(thumb);
									adapter();
								}
							});
						}
					}
				}
			}
		});
	}

	private void adapter() {
		glvma = new GroupListViewMessageAdapter(this,gmi);
		glvma.notifyDataSetChanged();
		listViewMessage.setAdapter(glvma);
		registerClickCallback();
	}

	private void showImage(byte[] data) {
		Bitmap bitmap = BitmapFactory.decodeByteArray(data , 0, data.length);
		((ImageView)findViewById(R.id.groupimage)).setImageBitmap(bitmap);
	}

	private OnClickListener buttonListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
			case R.id.createmessage_addmessage_button:
				if (member) {
					addGroupMessage();
				} 
				break;
			case R.id.joingroup_button:
				if (!member) {
					joinGroup();
				} else {
					leaveGroup();
				}
				break;
			case R.id.add_event_button:
				addEvent(); 
				break;	
			}
		}
	};


	/**
	 * Make item in listview clickable
	 */
	private void registerClickCallback() {
		// TODO Auto-generated method stub
		listViewMessage.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View viewClicked, int position, long id) {					

				LinearLayout messageCommentsll = (LinearLayout) viewClicked.findViewById(R.id.comments_group_message_linearlayout);

				if(messageCommentsll.getVisibility()==View.VISIBLE){
					messageCommentsll.setVisibility(View.GONE);
				}else{
					messageCommentsll.setVisibility(View.VISIBLE);
				}
			}
		});
	}

	/**
	 * Get you to create message
	 */
	private void addGroupMessage() {
		// TODO Auto-generated method stub
		Intent createMessage = new Intent(this, CreateGroupMessageActivity.class); //changed to create group message /anders
		createMessage.putExtra(GROUP_NAME, groupName);
		startActivity(createMessage);
	}

	/**
	 * get you to create event
	 */
	private void addEvent() {
		//TODO ändra till CreateGroupEventActivity //changed to create event /anders
		Intent addEvent = new Intent(this, CreateGroupEventActivity.class); 
		addEvent.putExtra(GROUP_NAME, groupName);
		startActivity(addEvent);
	}

	private void joinGroup() {
		ParseRelation<ParseObject> relation = ParseUser.getCurrentUser().getRelation("group");
		relation.add(parseGroup);
		ParseUser.getCurrentUser().saveInBackground(new SaveCallback() {

			@Override
			public void done(ParseException e) {
				if (e == null) {
					Toast.makeText(GroupActivity.this, R.string.joingroup, Toast.LENGTH_LONG).show();
					member();
				}
			}
		});
	}

	private void member() {
		findViewById(R.id.createmessage_addmessage_button).setVisibility(View.VISIBLE);
		member = true;
		joinLeaveGroup.setText(R.string.leavegroup_button_text);
	}

	private void leaveGroup() {

		ParseRelation<ParseObject> relation = ParseUser.getCurrentUser().getRelation("group");
		relation.remove(parseGroup);
		ParseUser.getCurrentUser().saveInBackground(new SaveCallback() {

			@Override
			public void done(ParseException e) {
				if (e == null) {
					leftGroup();
				}
			}
		});

	}

	private void leftGroup() {
		Toast.makeText(GroupActivity.this, R.string.leavegroup, Toast.LENGTH_LONG).show();
		findViewById(R.id.createmessage_addmessage_button).setVisibility(View.GONE);
		joinLeaveGroup.setText(R.string.joingroup_button_text);
		member = false;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.group, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
