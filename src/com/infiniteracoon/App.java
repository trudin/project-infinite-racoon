package com.infiniteracoon;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseACL;

public class App extends Application { 

    @Override public void onCreate() { 
        super.onCreate();
        Parse.initialize(this, "AkMIzOWxN5GPly0F6cIaij4ghrDMlG7h00B4wrcm",
        		"E6i88A5UvBND0jPMJAnghwDcUDvn9qqbUuJ78iqz");
        // ParseUser.enableAutomaticUser();
        ParseACL defaultACL = new ParseACL();
 
        // If you would like all objects to be private by default, remove this
        // line.
        defaultACL.setPublicReadAccess(true);
 
        ParseACL.setDefaultACL(defaultACL, true);
    }
} 