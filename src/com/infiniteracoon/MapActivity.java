package com.infiniteracoon;


import java.util.ArrayList;
import java.util.List;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;

public class MapActivity extends DrawerActivity implements 
			GooglePlayServicesClient.ConnectionCallbacks,
			GooglePlayServicesClient.OnConnectionFailedListener,
			ThumbsDownloadedListener
			{
	
	// Google play services variables
	private GoogleMap googleMap;
	private LocationClient locClient;
	private Location location;
	private LatLngBounds viewport;
	// Placeholder förstalånggatan för testing, ska nog innehålla defaultposition, kanske sverige?
	private LatLng point = new LatLng(57.699743, 11.946205);
	
	// Activity variables
	private List<ParseObject> groups;
	private boolean isFirstStart = true;
	private Marker addGroupMarker;
	
	private ImageView help;
	private int helpCount = 1;
	private Menu menu; //get toolbar
	private boolean helpIsShown  =false; 
	
	
	// Names for putExtra to pass position data to other activities.
	private final static String LATITUDE = "com.infiniteracoon.LATITUDE";
	private final static String LONGITUDE = "com.infiniteracoon.LONGITUDE";
	private final static String NEW_GROUP_MARKER = "Tryck på markören för att skapa grupp";
	private final static String NEW_GROUP_MOVE_MARKER = "Tryck länge för att flytta markören";
	private final static String MARKER_GROUP_NAME = "name";
	private final static int CREATE_GROUP_REQUEST = 100;	
	
	private OnClickListener ocl = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			//Prevent user from creating a marker, if not logged in
			if (ParseUser.getCurrentUser() != null) {
				
				// Get center of map position.
				point = googleMap.getCameraPosition().target;
			
				// Add a temporary marker that the user can click to create a group on this pos.
				addGroupMarker = googleMap.addMarker(new MarkerOptions()
	        		.position(point)
	        		.draggable(true)
	        		.title(NEW_GROUP_MARKER)
	        		.snippet(NEW_GROUP_MOVE_MARKER)
						);
			
				addGroupMarker.showInfoWindow();	
			}else {
				Toast toast = Toast.makeText(MapActivity.this , "Du måste logga in för att skapa en grupp", 2);
				toast.show();
			}
		}
	};
	
	private OnMarkerClickListener omcl = new OnMarkerClickListener() {
		
		@Override
		public boolean onMarkerClick(Marker clickedMarker) {
			if (clickedMarker.getTitle().equals(NEW_GROUP_MARKER)) {
				//Kollar om personen är inloggad
				ParseUser currentUser = ParseUser.getCurrentUser();
				if (currentUser == null) {
					showNoticeDialog();
				} else {
					Intent i = new Intent(MapActivity.this, CreateGroupActivity.class);
					i.putExtra(LATITUDE, clickedMarker.getPosition().latitude);
					i.putExtra(LONGITUDE, clickedMarker.getPosition().longitude);
					startActivityForResult(i, CREATE_GROUP_REQUEST);
				}
			} else {
				Intent i = new Intent(MapActivity.this, GroupActivity.class);
				i.putExtra(MARKER_GROUP_NAME, clickedMarker.getTitle());
				startActivity(i);
			}
			return true;
		}
	};
	
	private OnCameraChangeListener cameraChangedListener = new OnCameraChangeListener() {
		
		@Override
		public void onCameraChange(CameraPosition arg0) {
			viewport = googleMap.getProjection().getVisibleRegion().latLngBounds;  
			point = viewport.getCenter();
			setupMarkers();		
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);
		
		Toolbar toolbar = (Toolbar) findViewById(R.id.map_toolbar);	    
	    setSupportActionBar(toolbar);
	    getSupportActionBar().setDisplayShowTitleEnabled(false);
	    
	    groups = new ArrayList<ParseObject>();
	    addGroupsByFocusedArea();
	    viewport = new LatLngBounds(point, point);
	    
	    locClient = new LocationClient(this, this, this);			    
	    
        setUpMapIfNeeded();
		
		findViewById(R.id.map_addgroup_button).setOnClickListener(ocl);
		
		
		this.help= (ImageView)this.findViewById(R.id.map_help_image);
		this.help.setImageResource(R.drawable.help);

		
	this.help.setOnClickListener(new View.OnClickListener() {        
        @Override
           public void onClick(View view) {
        	//if click on help screen
            if (view == findViewById(R.id.map_help_image)) { 
            	
            	helpCount =1; 
            	help(false); //tell helpmethod to hide image
            	
            
            }
        }
	});
	}

	protected void onStart() {
		super.onStart();
		locClient.connect();
	}

	protected void onStop() {
		super.onStop();
		locClient.disconnect();
	}
	
    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

	private void addGroupsByFocusedArea() {
		if (point != null) {
			ParseGeoPoint userLocation = new ParseGeoPoint(point.latitude, point.longitude);
			ParseQuery<ParseObject> query = ParseQuery.getQuery("Group");
			query.whereWithinKilometers("location", userLocation, 10);
			// query.setLimit();
			query.findInBackground(new FindCallback<ParseObject>() {
				
				@Override
				public void done(List<ParseObject> objects, ParseException e) {
					groups = objects;
					// Place markers
					setupMarkers();
				}
			});
		}
	}
	
	private void help(boolean showHelp){
		
		ImageView help_image = (ImageView) findViewById(R.id.map_help_image);
		helpIsShown = showHelp; 
		
		if (!helpIsShown){
			helpCount = 1; //hide
		} else {
			helpCount = 2; //Show
		}
		
		switch (helpCount){
		case 1:  
			help_image.setVisibility(View.INVISIBLE);
			//Toast.makeText(this, "hide help "+ helpCount, Toast.LENGTH_SHORT).show();
			menu.getItem(1).setIcon(getResources().getDrawable(R.drawable.ic_help));
			break;
			
		case 2:  
			help_image.setVisibility(View.VISIBLE);
			menu.getItem(1).setIcon(getResources().getDrawable(R.drawable.ic_close));
			//Toast.makeText(this, "show help "+ helpCount, Toast.LENGTH_SHORT).show();
			break;	
			
		default:  
				Toast.makeText(this, "default "+ helpCount, Toast.LENGTH_SHORT).show(); 
			break;
		}	
	
	}
	
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (googleMap == null) {
            // Try to obtain the map from the SupportMapFragment.
        	googleMap = ((MapFragment)getFragmentManager().findFragmentById(R.id.map_fragment)).getMap();
            // Check if we were successful in obtaining the map.
            if (googleMap != null) {
                setupMap();
            }
        }
    }
	
	private void setupMap() {
		// TODO - setupmap if null - kolla så det gick bra att hämta kartan innan vi börjar manipulera den
		// TODO - centrera kartan, kanske via grupper som man själv skapat?		
		
		// Hämtar googleplay-kartan från xml. Där är den ett MapFragment som man kör getMap på
		// som returnerar en GoogleMap för att kunna köra metoder på den. (GoogleMaps kan inte 
		// instantieras direkt)
		
		// Karttyp: väg, sattelit, etc
		googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
		
		// Visar användarens position. Kräver permissions i manifestet. 
		googleMap.setMyLocationEnabled(true);
		
		// Visar info inifrån stora byggnader, default är true. False kanske hjälper performance
		// men om man vill kunna ha en grupp specifikt inomhus så kanske man vill ha det true.
		// Typ samma med att visa buildings i 3d.
		googleMap.setIndoorEnabled(false);
		googleMap.setBuildingsEnabled(false);
	
		// Talar om för kartan att delar runt kanten kan vara dolda så den inte visar UI-element
		// i paddingen, bra för att trycka ner knappar som annars hade dolts av vårt sökfält.
		// Ett alternativ till detta är att gömma UI-element och skriva våra egna.
		googleMap.setPadding(0, 150, 0, 0);
		googleMap.getUiSettings().setZoomControlsEnabled(false);
		
		// Override default marker behavor (to open info-window) when clicked.
		googleMap.setOnMarkerClickListener(omcl);		
		
		// Listener för om användaren flyttar kartan, skapar en bounds med den nya synliga kartan
		// och laddar in markers för de grupper som bör synas.
		googleMap.setOnCameraChangeListener(cameraChangedListener);	
		
	
	}

	private void setupMarkers() {
		
		// Check if there are any groups on the visible map
		List<ParseObject> visibleMarkers = getGroupsByViewport();
		
		if (!visibleMarkers.isEmpty()) {						
			GetGroupThumbsTask getThumbs = new GetGroupThumbsTask(visibleMarkers, this);
			getThumbs.execute();
		}
	}

	@Override
	public void thumbsDownsloaded(ArrayList<ParseObject> groups,
			ArrayList<Bitmap> thumbs) {	
		// Ifall man vill implementa detta interfaces nån annanstans i appen men göra nåt annat med
		// thumbsen så skickar jag bara vidare detta till nästa metod.
		placeMarkers(groups, thumbs);
	}

	private void placeMarkers(ArrayList<ParseObject> visibleMarkers, ArrayList<Bitmap> thumbs) {
		// Ta bort tidigare markörer.
		googleMap.clear();
		
		LayoutInflater inflater = (LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);		
		RelativeLayout infoWindow = new RelativeLayout(this);
		
		// Inflate:a menyn med rätt info. Kan sätta texten och bild till rätt grupp i en loop av
		// sökresultaten från databasen.
		inflater.inflate(R.layout.map_info_window, infoWindow, true);
		
		// Measure how much space the layout needs.
		infoWindow.measure(MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED), 
				MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
		infoWindow.layout(0, 0, infoWindow.getMeasuredWidth(), infoWindow.getMeasuredHeight()); 
		
		
		// Add information from group.
		TextView nameTv = (TextView)infoWindow.findViewById(R.id.map_info_namn_text);
		TextView infoTv = (TextView)infoWindow.findViewById(R.id.map_info_info_text);
		ImageView imageIv = (ImageView)infoWindow.findViewById(R.id.map_info_image);
		
		//Create a bitmap with the right size
		Bitmap bitmap = Bitmap.createBitmap(infoWindow.getMeasuredWidth(), 
				infoWindow.getMeasuredHeight(), 
				Bitmap.Config.ARGB_8888);
		
		//Create a canvas with the specified bitmap to draw into
		Canvas c = new Canvas(bitmap);
		
		// For each Group that is positioned on the visible map, create a marker.
		for (int i = 0; i < visibleMarkers.size(); i++) {			
		
			ParseObject g = visibleMarkers.get(i);
			// Set the textview texts.

			nameTv.setText(g.getString("name"));
			infoTv.setText(g.getString("description"));
			imageIv.setImageBitmap(thumbs.get(i));		
			
			infoWindow.draw(c);
			
			point = new LatLng(g.getParseGeoPoint("location").getLatitude(), 
					g.getParseGeoPoint("location").getLongitude());
			
			// Markör
			googleMap.addMarker(new MarkerOptions()
			.position(point)
			.anchor(0.5f, 1.0f)
			.title(g.getString("name"))
			.icon(BitmapDescriptorFactory.fromBitmap(bitmap)));				
			
		}
	}

	private List<ParseObject> getGroupsByViewport() {
		
		LatLng pos;
		
		List<ParseObject> sortedList = new ArrayList<ParseObject>(); 
		for (ParseObject g : groups) {
			
			pos = new LatLng(g.getParseGeoPoint("location").getLatitude(), 
							g.getParseGeoPoint("location").getLongitude());
			
			if (viewport.contains(pos)) {				
				sortedList.add(g);				
			}
		}
		return sortedList;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// Check which request we're responding to
	    if (requestCode == CREATE_GROUP_REQUEST) {
	        // Make sure the request was successful
	        if (resultCode == RESULT_OK) {
	        	Bundle extras = data.getExtras();
	            point = new LatLng(extras.getDouble(CreateGroupActivity.LATITUDE), extras.getDouble(CreateGroupActivity.LONGITUDE));
	            ParseQuery<ParseObject> query = ParseQuery.getQuery("Group");
				query.whereEqualTo("name", extras.getString(CreateGroupActivity.NAME));				
				query.findInBackground(new FindCallback<ParseObject>() {
					
					@Override
					public void done(List<ParseObject> objects, ParseException e) {
						if (e == null) {
							groups.add(objects.get(0));							
							googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(point, 13));
						}
					}
				});	            
	        }
	    }		
	}

	@Override
    protected void onNewIntent(Intent intent) {
        handleSearchIntent(intent);
    }
	
	private void handleSearchIntent(Intent intent) {
		
	    if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
	        String query = intent.getStringExtra(SearchManager.QUERY);
	        
	        if (query.startsWith("#"))	{
		        searchHashtags(query.substring(1)); // Sök utan prefix men bara hashtags.
		        
	        } else if (query.startsWith("@")) {
				moveToAddress(query.substring(1)); // Gå till adress.
		        
			} else { // Om användaren inte tryckt på suggestion, sök först på hashtag, sen adress.
				searchHashtags(query);	
			}   
	    }
	}

	private void moveToAddress(String query) {
		String[] position = query.split("@");
		point = new LatLng(Double.parseDouble(position[0]), Double.parseDouble(position[1]));
		googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(point, 13));
		addGroupsByFocusedArea();
		
	}

	private void searchForAddress(String query) {
		
		Address address = new Address(null);
		try {				
			address = new Geocoder(this).getFromLocationName(query, 1).get(0);
		} catch (Exception e) {
			// TODO: handle exception
		}
		if (address.hasLatitude()) {
			point = new LatLng(address.getLatitude(), address.getLongitude());
			googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(point, 14));
			addGroupsByFocusedArea();
			
		} else Toast.makeText(this, "Hittade ingenting", Toast.LENGTH_LONG).show();
	}

	private void searchHashtags(final String searchString) {
		
		// Ta bort markers om det finns några gamla som skräpar
		googleMap.clear();		
		
		ParseQuery<ParseObject> hashtagQ = ParseQuery.getQuery("Hashtag");
		hashtagQ.whereEqualTo("hashtag", searchString);
		hashtagQ.findInBackground(new FindCallback<ParseObject>() {
			
			// Söker upp hashtags från hashtagtabellen.
			@Override
			public void done(List<ParseObject> objects, ParseException e) {
					if (e == null && !objects.isEmpty()) {
						ParseRelation<ParseObject> groupsWithHashtag = objects.get(0).getRelation("groups");
						ParseQuery<ParseObject> groupsQ = groupsWithHashtag.getQuery(); 
						groupsQ.setLimit(10);
						groupsQ.findInBackground(new FindCallback<ParseObject>() {
							
							// Om vi fått en träff så hämtar vi dom grupperna som är relaterade till hashtaggen.
							@Override
							public void done(List<ParseObject> objects, ParseException e) {
								
								if (e == null && !objects.isEmpty()) {
										groups = objects;
										// TODO - create bounds - zoom out to closest that contains all
										point = new LatLng(groups.get(0).getParseGeoPoint("location").getLatitude(), 
												groups.get(0).getParseGeoPoint("location").getLongitude());
										googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(point, 12));
								} else {
									searchForAddress(searchString); // Om sökningen inte går igenom (bör inte hända men man vet ju aldrig) 
								}							
							}
						});
					} else {
						searchForAddress(searchString); // Om det inte finns någon hashtag -> sök på adress.
					}
				}
		});
    	
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.map, menu);
		
	    // Get the SearchView and set the searchable configuration
	    SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
	    SearchView searchView = (SearchView) menu.findItem(R.id.action_map_search).getActionView();
	    // Assumes current activity is the searchable activity
	    searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
	    searchView.setQueryRefinementEnabled(true);
//	    searchView.setIconifiedByDefault(false);
		
	    this.menu = menu; //toolbar, to change icon on help
		
		return true;
	}

	      
        
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.

		//Get item
		int id = item.getItemId();	
		
		//if click help button
		if (id == R.id.action_help){  
			helpCount = helpCount+1; //inc click
			
			if (helpCount ==2){
				help(true); //tell helpmethod to show image
			}else {
				helpCount = 1;
				help(false); //tell helpmethod to hide image
			}
					
		}
		
	
		if (mDrawerToggle.onOptionsItemSelected(item)) {
	        return true;
	    }
		return super.onOptionsItemSelected(item);
	}

	       
	

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onConnected(Bundle arg0) {
		location = locClient.getLastLocation();
		
	    addGroupsByFocusedArea();
		
		if (location != null && isFirstStart) {
			point = new LatLng(location.getLatitude(), location.getLongitude());
			
			// Zooma in och flytta "kameran" till position
			// googleMap.moveCamera(CameraUpdateFactory.newLatLng(point));		
			googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(point, 13));
			
			// Dont update map camera to user position if the app is only suspended.
			isFirstStart = false;
		}
	}

	@Override
	public void onDisconnected() {
		// TODO Auto-generated method stub		
	}

    
}
