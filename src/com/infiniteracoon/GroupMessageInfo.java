package com.infiniteracoon;

import android.graphics.Bitmap;

public class GroupMessageInfo {

	private String surename;
	private String lastname;
	private String title;
	private String message;
	private String date;
	private String comments;
	private Bitmap thumb;
	
	
	public GroupMessageInfo(String surename, String lastname, String title, String message, 
									String date, String comments){
		setSurename(surename);
		setLastname(lastname);
		setTitle(title);
		setMessage(message);
		setDate(date);
		setComments(comments);
	}


	public String getSurename() {
		return surename;
	}


	public void setSurename(String surename) {
		this.surename = surename;
	}


	public String getLastname() {
		return lastname;
	}


	public void setLastname(String lastname) {
		this.lastname = lastname;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	public String getDate() {
		return date;
	}


	public void setDate(String date) {
		this.date = date;
	}


	public String getComments() {
		return comments;
	}


	public void setComments(String comments) {
		this.comments = comments;
	}


	public Bitmap getThumb() {
		return thumb;
	}


	public void setThumb(Bitmap thumb) {
		this.thumb = thumb;
	}
	
}
