package com.infiniteracoon;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class CreateGroupEventActivity extends ActionBarActivity {

	private String groupName; 

	private OnClickListener buttonListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			//checks witch button is pressed
			switch (v.getId()) {
			case R.id.create_event_decline_button:
				finish();
				break;

			case R.id.create_event_accept_button:
				saveEvent();
				break;

			default:
				break;
			}
		}
	};


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_group_event);
		//sets groupName to variable
		groupName = getIntent().getExtras().getString("com.infiniteracoon.GROUP_NAME");
		//sets the text for groupeName
		((TextView)findViewById(R.id.create_event_groupname)).setText(groupName);
		//add toolbar 
		Toolbar toolbar = (Toolbar) findViewById(R.id.create_groupevent_toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayShowTitleEnabled(false);

		// Set onClickListeners
		findViewById(R.id.create_event_accept_button).setOnClickListener(buttonListener);
		findViewById(R.id.create_event_decline_button).setOnClickListener(buttonListener);	
	}

	/**
	 * saves event
	 */
	private void saveEvent(){
		ParseObject message = new ParseObject("Event");
		message.put("message", ((EditText)findViewById(R.id.create_event_message_edit)).getText().toString());
		message.put("group", groupName);
		message.saveInBackground(new SaveCallback() {

			@Override
			public void done(ParseException e) {
				if (e == null) {
					Toast.makeText(CreateGroupEventActivity.this, "Eventet sparat", Toast.LENGTH_LONG).show();
					finish();
				} else {
					Toast.makeText(CreateGroupEventActivity.this, "Eventet ej sparat: " + e.getMessage(), Toast.LENGTH_LONG).show();
				}
			}
		});
	}



	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.create_group_event, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
